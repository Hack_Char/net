#!/usr/bin/python3
#
#
# bootstrap dockerized samba AD
#

import os
import subprocess

if not os.path.isfile("/srv/etc/smb.conf"):
# need to provision
  print("--> Provisioning AD")
#  subprocess.run(["/srv/bin/samba-tool","domain","provision","--server-role=dc","--use-rfc2307","--dns-backend=SAMBA_INTERNAL",
#                  "--realm={}".format(os.environ['SAMBA_DOCKER_REALM']),"--domain={}".format(os.environ['SAMBA_DOCKER_DOMAIN']),
#                  "--adminpass={}".format(os.environ['SAMBA_DOCKER_PASSWORD']),"--host-ip={}".format(os.environ['SAMBA_IP']) ])
  subprocess.run(["/srv/bin/samba-tool","domain","provision","--server-role=dc","--use-rfc2307","--dns-backend=SAMBA_INTERNAL",
                  "--realm={}".format(os.environ['SAMBA_DOCKER_REALM']),"--domain={}".format(os.environ['SAMBA_DOCKER_DOMAIN']),
                  "--adminpass={}".format(os.environ['SAMBA_DOCKER_PASSWORD']) ])
#  subprocess.run(["/bin/cp","/srv/private/krb5.conf","/etc"])

print("--> Running Samba AD DC")
subprocess.run(["/srv/sbin/samba","-i","-M","standard","--debug-stderr","-d","2"])
#subprocess.run(["/usr/bin/strace","-o","/srv/private/strace.log","-e","trace=%file","-f","/srv/sbin/samba","-i","-M","standard","--debug-stderr","-d","2"])

print("--> Exiting ...")

