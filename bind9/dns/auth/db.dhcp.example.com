$TTL    604800
@       IN      SOA     dns.example.com. root.example.com. (
                              1         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;
; dhcp.example.com
;
	IN	NS	dns.example.com.
;
;
@	IN	A	10.1.1.1


