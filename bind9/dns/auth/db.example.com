$TTL    604800
@       IN      SOA     dns.example.com. root.example.com. (
                              1         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;
; example.com
;
			IN	NS	dns
;
;dns			IN	A	172.29.0.3
dns			IN	A	10.1.1.1
;
; must forward AD DNS to the samba DC1.AD server
ad.example.com.		IN	NS	dc1.ad.example.com.
dc1.ad.example.com.	IN	A	172.29.0.4


