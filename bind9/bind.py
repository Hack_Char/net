#!/usr/bin/python3

import random
import base64

secret_str=bytes([random.randint(0,254) for i in range(64)])
secret=base64.b64encode(secret_str)

print('key "key" {')
print("\talgorithm hmac-sha512;")
print("\tsecret \"{}\";".format(secret.decode("utf-8")))
print('};')

