# Network Services

------------------------------------
NOTICE: NOT A RELEASED LEVEL OF CODE
------------------------------------

Goal: Provide Active Directory, DNS and DHCP to local network

* ISC DHCP 4.4.2
* ISC BIND9 9.1.3
* SAMBA AD 4.10.8

## Quick Start

Clone repository, install docker and run ./startup.sh  

### Details
 
Modify settings to customize to your site. Source a local environment variable script to set sensitive items like Administrator password.
Use a VM to test these services. Configure test host VM to connect to the docker VM and request DHCP, AD, etc.
Using 'docker-compose-pre.yml' to run a configure container that clones needed repositories and generates a shared key. Will run in order
if you use startup.sh otherwise can just run 'docker-compose up' after running the common container once. Ideally common container would
check for newer released versions and kick off re-build of services when needed.

## Active Directory

Samba AD service. Configures interactively on first run of container from environment variables (see docker-compose.yml).
Full access via samba-tool binary inside container (example):
'''docker exec -it net_dc1_1 /srv/bin/samba-tool user list'''
Use 'realmd' to join debian-based linux machines to the domain. Configure to allow domain logins or not. Default Domain User will not be able to do a domain join (can use defaul Domain Admin Administrator).
Otherwise need to connect with a windows host to alter the AD such that needed priveleges are available to modify computer accounts, etc for domain joining.
Use 'ssh <domain-joined-hose> -l <domain-user>@<realm short name>' to login with AD credentials.
Can use samba-tool to modify samba DNS under ad.example.com (uses samba internal name server). By default only dc1 will appear under ad.example.com.
Complicated AD configuration should use a windows client to connect and modify AD. Samba only has rudimentary linux CLI ability for this.

## DNS

Using BIND9 and two levels of server. Note that Samba AD contains an internal DNS server.

* When troubleshooting BIND9, set flag -g instead of -f to output more descriptive error messages to stderr.

### Recursive

Recursive BIND9 resolves all internet addresses and forwards/caches to authorative local server. Does not contain shared key for dynamic updates.

### Authorative

Includes DNS records for local network beyond what is maintained by Samba AD. Uses a shared key for dynamic updates.

## DHCP

Only reason to include DHCP here is to allow dynamic DNS updates for local hosts. By default pushes updates to 'dhcp.example.com'.
Use reservation to place known-good hosts under 'example.com'. Configure hostname first and it will appear as '<hostname>.dhcp.example.com'.

## To Do

* DNS update failing in AD container
* How to record commit hash compiled against? Text file inside image?
* Make fully configurable from script?
  * Currently need to edit configuration files for most sites.
* Samba file share joined to AD

## Current Default

(Authorative DNS)<->(DHCP)
                 <->(AD)
                 <->(Recursive DNS)

10.1.1.0/24 'example.com'
All dhcp placed in 'dhcp.example.com'
AD is AD.EXAMPLE.COM with short name EXAMPLEAD running on 'dc1'
10.1.1.1 is the docker host and appears as 'dns.example.com' as well as 'dc1.ad.example.com'


